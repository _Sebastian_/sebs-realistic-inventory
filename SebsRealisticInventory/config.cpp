class CfgPatches
{
	class SebsRealisticInventory
	{
		requiredAddons[]=
		{
			"DZ_Data",
			"DZ_Characters",
			"DZ_Characters_Backpacks",
			"DZ_Characters_Vests",
			"DZ_Gear_Containers",
			
			"Windstride_Clothing",
			"Munghardshikingbag"
		};
	};
};

class CfgMods
{
	class SebsRealisticInventory
	{
		type = "mod";

		class defs
		{
			class gameScriptModule
			{
				value = "";
				files[] = { "SebsRealisticInventory/scripts/3_game" };
			};

			class worldScriptModule
			{
				value = "";
				files[] = { "SebsRealisticInventory/scripts/4_world" };
			};
		};
	};
};

class CfgVehicles
{
	class Inventory_Base;
	class Container_Base;
	class Clothing_Base;
	class Clothing: Clothing_Base
	{
	};

	class PlateCarrierPouches: Container_Base
	{
		itemSize[]={4,3};//6,4
		itemsCargoSize[]={4,3};//6,4
	};

// HEAD
	class NBCHoodBase: Clothing
	{
		itemSize[]={2,1};
	};


// TOPS
	class Sweater_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Shirt_ColorBase: Clothing
	{
		itemsCargoSize[]={1,2};
	};
	class TShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Hoodie_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class TacticalShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class HikingJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Raincoat_ColorBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemSize[]={2,1};
		itemsCargoSize[]={0,0};
	};
	class TorsoCover_Improvised: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class M65Jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class TTsKOJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,5};
	};
	class GorkaEJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class RidersJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class WoolCoat_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class TrackSuitJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class PoliceJacket: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class PoliceJacketOrel: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class ParamedicJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class FirefighterJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class PrisonUniformJacket: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class MiniDress_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class QuiltedJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class BomberJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class LeatherJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class HuntingJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class MedicalScrubsShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class LabCoat: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class NurseDress_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class USMCJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Blouse_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class NBCJacketBase: Clothing
	{
		attachments[]=
		{
			"Body"
		};
		itemSize[]={2,1};
		itemsCargoSize[]={0,0};
	};
	class DenimJacket: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class TelnyashkaShirt: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class ChernarusSportShirt: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class JumpsuitJacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class BDUJacket: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class ManSuit_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class WomanSuit_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class LeatherShirt_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Chainmail: Clothing
	{
		itemsCargoSize[]={0,0};
		quickBarBonus=0;
	};


// HANDS
	class NBCGloves_ColorBase: Clothing
	{
		itemSize[]={2,1};
	};


// LEGS
	class Jeans_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class CargoPants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class TTSKOPants: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class HunterPants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class CanvasPants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class CanvasPantsMidi_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class TrackSuitPants_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class GorkaPants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class PolicePants: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class PolicePantsOrel: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class ParamedicPants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class FirefightersPants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class PrisonUniformPants: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class LeatherPants_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class MedicalScrubsPants_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class USMCPants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class SlacksPants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class BDUPants: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class NBCPantsBase: Clothing
	{
		attachments[]=
		{
			"Legs"
		};
		itemSize[]={2,1};
		itemsCargoSize[]={0,0};
		quickBarBonus=0;
	};
	class Breeches_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class ShortJeans_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Skirt_ColorBase: Clothing
	{
		itemsCargoSize[]={3,1};
	};
	class JumpsuitPants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class LegsCover_Improvised: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Chainmail_Leggings: Clothing
	{
		itemsCargoSize[]={0,0};
		quickBarBonus=0;
	};


// FEET
	class NBCBootsBase: Clothing
	{
		attachments[]=
		{
			"Feet"
		};
		itemSize[]={2,1};
	};
	class Wellies_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		weight=1700;
		heatIsolation=0.5;
	};


// VESTS
	class SmershVest: Clothing
	{
		itemsCargoSize[]={5,4};
	};
	class PressVest_ColorBase: Clothing
	{
		itemsCargoSize[]={4,5};
	};
	class UKAssVest_ColorBase: Clothing
	{
		itemsCargoSize[]={6,4};
	};
	class HighCapacityVest_ColorBase: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class LeatherStorageVest_ColorBase: Clothing
	{
		itemsCargoSize[]={4,4};
	};
	class HuntingVest: Clothing
	{
		itemsCargoSize[]={4,4};
	};


// BAGS
	class TaloonBag_ColorBase: Clothing
	{
		itemsCargoSize[]={5,6};
	};
	class TortillaBag: Clothing
	{
		itemsCargoSize[]={6,7};
	};
	class CourierBag: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class FurCourierBag: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class ImprovisedBag: Clothing
	{
		itemsCargoSize[]={5,6};
	};
	class FurImprovisedBag: Clothing
	{
		itemsCargoSize[]={5,6};
	};
	class DryBag_ColorBase: Clothing
	{
		itemsCargoSize[]={6,7};
	};
	class HuntingBag: Clothing
	{
		itemsCargoSize[]={6,6};
	};
	class MountainBag_ColorBase: Clothing
	{
		itemsCargoSize[]={6,8};
	};
	class SmershBag: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class ChildBag_ColorBase: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class LeatherSack_ColorBase: Clothing
	{
		itemsCargoSize[]={6,7};
	};
	class AssaultBag_ColorBase: Clothing
	{
		itemsCargoSize[]={5,6};
	};
	class CoyoteBag_ColorBase: Clothing
	{
		itemsCargoSize[]={6,7};
	};
	class AliceBag_ColorBase: Clothing
	{
		itemsCargoSize[]={7,8};
	};


//Apokot
	class apokot_wellies_ColorBase: Clothing
	{
		attachments[]=
		{
			"Knife"
		};
		weight=1200;
		heatIsolation=0.5;
	};
	class apokot_ussr_bag_ColorBase: Clothing
	{
		itemsCargoSize[]={6,5};
	};


//Zeroy
	class Zeroy_fishing_Pants_Camo: NBCPantsBase
	{
		itemsCargoSize[]={0,0};
	};


//Windstride
	class Leather_Cloak_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};

	class Medium_Sleeves_Shirt: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Military_Sweater: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Layered_Shirt_Base: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Winter_Parka_Base: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Winter_Parka_Green: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Wool_GreatCoat_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};

	class Galife_Pants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Kneepads_Jeans_Base: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Leggings_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Skinny_Jeans_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};

	class Canvas_Backpack_Base: Clothing
	{
		itemsCargoSize[]={5,6};
	};


//Munghard
	class pilotjacket_mung: Clothing
	{
		itemsCargoSize[]={3,2};
	};

	class milipants_mung: Clothing
	{
		itemsCargoSize[]={4,3};
	};
		class stalkerpants_mung: Clothing
	{
		itemsCargoSize[]={4,3};
	};

	class dappervest_mung: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class bikervest_mung: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class tacticalvest_mung: Clothing
	{
		itemsCargoSize[]={5,5};
	};
	class tacvest_mung: Clothing
	{
		itemsCargoSize[]={0,0};
	};

	class jaakari_Colorbase_mung: Clothing
	{
		itemsCargoSize[]={6,8};
	};
	class bag_6B38_Colorbase_mung: Clothing
	{
		itemsCargoSize[]={7,8};
	};
	class Hikingbagmung_ColorBase: Clothing
	{
		itemsCargoSize[]={7,8};
	};


//Loft
	class Loftd_bugbag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,5};
	};
	class Loftd_bunnybag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,5};
	};
	class Loftd_elliebag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,6};
	};
	class Loftd_hollybag_ColorBase: Clothing
	{
		itemsCargoSize[]={6,6};
	};
	class Loftd_leatherBag_ColorBase: Clothing
	{
		itemsCargoSize[]={4,4};
	};
	class Loftd_reinabag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={4,7};
	};
	class Loftd_slackerbag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={6,6};
	};
	class Loftd_survivorbag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,6};
	};
	class Loftd_teddybag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,5};
	};
	class Loftd_Town_bag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,5};
	};
	class Loftd_Townbag_pouch_ColorBase: Container_Base
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_Townbag_pouch2_ColorBase: Container_Base
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_travelsbag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={8,5};
	};
	class Loftd_valeriebag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,5};
	};
	class Loftd_womensbag_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};

	class Loftd_Arcpants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Loftd_bakedpants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_cargopants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_cargoshorts_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_cyberpants_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_jeans: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_ririteopants_ColorBase: Clothing
	{
		itemsCargoSize[]={5,2};
	};
	class Loftd_tifan_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_westpants_ColorBase: Clothing
	{
		itemsCargoSize[]={5,3};
	};

	class Loftd_acronym_ColorBase: Clothing
	{
		itemsCargoSize[]={4,4};
	};
	class Loftd_adidasjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_aidansuit_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_aidansuit_pants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_albert_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_alexjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_armyjacket: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_bajka_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_bikerjacket: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_bomberjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_brian_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_Brianpug_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_bridgejacket: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_brundonjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_cybershirt_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Loftd_femalejacket_ColorBase: Clothing
	{
		itemsCargoSize[]={1,2};
	};
	class Loftd_gaptopsuit_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_gaptopsuit_jacket_CL_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_gaptopsuit_pants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_hoodedjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_jacketfranc_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_jackethen_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_jacketlong_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_leatherjacket: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Loftd_leatherjacketW: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_northjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_Reinajacket_ColorBase: Clothing
	{
		itemsCargoSize[]={1,2};
	};
	class Loftd_Reinapants_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Ririteojacket_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_roadiessuit_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_roadiessuit_pants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_shirt_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Loftd_shirtJ_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_suedejacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_waistcoat_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_windjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={4,4};
	};
	class Loftd_wolverine_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_wooljacket: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_wornjacket_ColorBase
	{
		itemsCargoSize[]={3,4};
	};

	class Loftd_bloodvest_ColorBase: Clothing
	{
		itemsCargoSize[]={7,5};
	};
	class Loftd_civilvest_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_huntervest_ColorBase: Clothing
	{
		itemsCargoSize[]={6,4};
	};

	class Loftd_blackpants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_carharttpants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Loftd_jeanspants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_Leggings_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Loftd_urbanpants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,5};
	};

	class Loftd_akito_jacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Loftd_akito_pants_ColorBase: Clothing
	{
		itemsCargoSize[]={5,2};
	};
	class Loftd_akito_bag: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_blacksuitshirt: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Loftd_blacksuitpants: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_Michonneshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Michonnepants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_MiniDress_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Loftd_Sheriffshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Sheriffpants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_wdpants_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_wdcoat_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};

	class Loftd_canadajacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_hoodie_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_plumpjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_pufferjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_sleevelessshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Stylishjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};

	class Loftd_dixonvest_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_Puffervest_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};

	class Loftd_Assaultbackpack_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,7};
	};
	class Loftd_shadowbag_ColorBase: TaloonBag_ColorBase
	{
		itemsCargoSize[]={5,6};
	};

	class Loftd_coloradopants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_Ironsightpants_ColorBase: Clothing
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_Ironsightpantsv2_ColorBase: Clothing
	{
		itemsCargoSize[]={4,2};
	};
	class Loftd_Ironsightpantsv3_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_militarysetpants_ColorBase: Clothing
	{
		itemsCargoSize[]={0,0};
	};
	class Loftd_tacticalpants_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};

	class Loftd_CHRSAT_jacket: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_CHRSAT_pants: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_WILD_jacket: Loftd_CHRSAT_jacket
	{
		itemsCargoSize[]={1,2};
	};
	class Loftd_WILD_pants: Loftd_CHRSAT_pants
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_dawnpants_ColorBase: Clothing
	{
		itemsCargoSize[]={5,2};
	};
	class Loftd_dawnjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};

	class Loftd_coloradojacket: Clothing
	{
		itemsCargoSize[]={3,4};
	};
	class Loftd_Ironsightjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_Ironsightshirt_ColorBase: Clothing
	{
		itemsCargoSize[]={2,2};
	};
	class Loftd_Ironsightwindbreaker_ColorBase: Clothing
	{
		itemsCargoSize[]={3,2};
	};
	class Loftd_militarysetjacket_ColorBase: Clothing
	{
		itemsCargoSize[]={1,2};
	};
	class Loftd_tacticaljacket_ColorBase: Clothing
	{
		itemsCargoSize[]={3,4};
	};

	class Loftd_militarysetvest_ColorBase: Clothing
	{
		itemsCargoSize[]={4,3};
	};
	class Loftd_bigpouch: Container_Base
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_bigpouch2: Container_Base
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_sidebag1: Container_Base
	{
		itemsCargoSize[]={3,3};
	};
	class Loftd_sidebag2: Container_Base
	{
		itemsCargoSize[]={2,3};
	};
	class Loftd_Middletopbag: Container_Base
	{
		itemsCargoSize[]={3,2};
	};
};
